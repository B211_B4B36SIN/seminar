package dao;

import entity.Author;

import java.util.List;


public interface AuthorDAO extends DAO<Author> {

    List<Author> findByLastName(String lastName);

}