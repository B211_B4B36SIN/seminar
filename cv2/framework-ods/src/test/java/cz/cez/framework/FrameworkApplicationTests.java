package cz.cez.framework;

import cz.cez.framework.model.TestPartner;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@AutoConfigureTestEntityManager
@RunWith(SpringRunner.class)
@DataJpaTest
class FrameworkApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private TestPartnerRepository partnerRepository;

	//integration test
	@Test
	public void whenFindByName_thenReturnPartner() {
		// given
		TestPartner partner = new TestPartner();
		partner.setFirstName("100");
		entityManager.persist(partner);
		entityManager.flush();

		// when
		TestPartner found = partnerRepository.getTestPartnerByFirstName("100");

		// then
		assertThat(found.getFirstName())
				.isEqualTo(partner.getFirstName());
	}

}
