package cz.cez.framework;

import cz.cez.framework.model.TestPartner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

@Component
public class TestPartnerService {
    @Autowired
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private TestPartnerRepository testPartnerRepository;

    TestPartner getTestPartnerByFirstName(String firstName){
        return testPartnerRepository.getTestPartnerByFirstName(firstName);
    }

    List<Object[]> getTestPartner(int partnerKey) {
        StoredProcedureQuery query = entityManager
                .createStoredProcedureQuery("ODS_API.GET_TEST_PARTNER")
                .registerStoredProcedureParameter(
                        1,
                        Integer.class,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        2,
                        String.class,
                        ParameterMode.OUT
                )
                .registerStoredProcedureParameter(
                        3,
                        String.class,
                        ParameterMode.OUT
                )
                .registerStoredProcedureParameter(
                        4,
                        String.class,
                        ParameterMode.OUT
                )
                .registerStoredProcedureParameter(
                        5,
                        Date.class,
                        ParameterMode.OUT
                )
                .registerStoredProcedureParameter(
                        6,
                        String.class,
                        ParameterMode.OUT
                )
                .setParameter(1, partnerKey);
        query.execute();

        return query.getResultList();

    }

    List<Object[]> getTestPartnerByCompName(String compName) {
        StoredProcedureQuery query = entityManager
                .createStoredProcedureQuery("ODS_API.GET_TEST_PARTNER_BY_COMP_NAME")
                .registerStoredProcedureParameter(
                        1,
                        String.class,
                        ParameterMode.IN
                )
                .registerStoredProcedureParameter(
                        2,
                        TestPartner.class,
                        ParameterMode.REF_CURSOR
                )
                .setParameter(1, compName);

        query.execute();

        return query.getResultList();
    }

    @Transactional
    public void insertWithQuery(TestPartner testPartner) {
        entityManager.createNativeQuery("INSERT INTO \"ODS_API\".\"TEST_PARTNER\" (PARTNER_KEY, DEL_FLAG, INS_PROCESS_ID, INS_DT, UPD_PROCESS_ID, UPD_DT, UPD_EFF_DT, SRC_UPD_DT, SRC_ID, SRC_SYS_ID, TP_PARTNER_KEY, COMP_NAME, FIRST_NAME, LAST_NAME, BRTH_DATE, DEATH_DATE) VALUES (?, ?, ?,  TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), ?,  TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'),  TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'),  TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), ?, ?, ?, ?, ?, ?,  TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'),  TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'))\n")
                .setParameter(1, testPartner.getPartnerKey())
                .setParameter(2, testPartner.getDelFlag())
                .setParameter(3, testPartner.getInsProcessId())
                .setParameter(4, testPartner.getInsDt())
                .setParameter(5, testPartner.getUpdProcessId())
                .setParameter(6, testPartner.getUpdDt())
                .setParameter(7, testPartner.getUpdEffDt())
                .setParameter(8, testPartner.getSrcUpdDt())
                .setParameter(9, testPartner.getSrcId())
                .setParameter(10, testPartner.getSrcSysId())
                .setParameter(11, testPartner.getTpPartnerKey())
                .setParameter(12, testPartner.getCompName())
                .setParameter(13, testPartner.getFirstName())
                .setParameter(14, testPartner.getLastName())
                .setParameter(15, testPartner.getBrthDate())
                .setParameter(16, testPartner.getDeathDate())
                .executeUpdate();
    }

    @Transactional
    public void updateWithQuery(TestPartner testPartner, int id) {
        entityManager.createNativeQuery("UPDATE \"ODS_API\".\"TEST_PARTNER\" SET PARTNER_KEY=?, DEL_FLAG=?, INS_PROCESS_ID=?, INS_DT=TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), UPD_PROCESS_ID=?, UPD_DT=TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), UPD_EFF_DT=TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), SRC_UPD_DT=TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), SRC_ID=?, SRC_SYS_ID=?, TP_PARTNER_KEY=?, COMP_NAME=?, FIRST_NAME=?, LAST_NAME=?, BRTH_DATE=TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), DEATH_DATE=TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS') WHERE PARTNER_KEY =? \n")
                .setParameter(1, testPartner.getPartnerKey())
                .setParameter(2, testPartner.getDelFlag())
                .setParameter(3, testPartner.getInsProcessId())
                .setParameter(4, testPartner.getInsDt())
                .setParameter(5, testPartner.getUpdProcessId())
                .setParameter(6, testPartner.getUpdDt())
                .setParameter(7, testPartner.getUpdEffDt())
                .setParameter(8, testPartner.getSrcUpdDt())
                .setParameter(9, testPartner.getSrcId())
                .setParameter(10, testPartner.getSrcSysId())
                .setParameter(11, testPartner.getTpPartnerKey())
                .setParameter(12, testPartner.getCompName())
                .setParameter(13, testPartner.getFirstName())
                .setParameter(14, testPartner.getLastName())
                .setParameter(15, testPartner.getBrthDate())
                .setParameter(16, testPartner.getDeathDate())
                .setParameter(17, id)
                .executeUpdate();
    }
}