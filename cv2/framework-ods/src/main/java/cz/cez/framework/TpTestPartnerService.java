package cz.cez.framework;

import cz.cez.framework.model.TpTestPartner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Component
public class TpTestPartnerService {
    @Autowired
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void insertWithQuery(TpTestPartner tpTestPartner) {
       entityManager.createNativeQuery("INSERT INTO \"ODS_API\".\"TP_TEST_PARTNER\" (TP_PARTNER_KEY, ID, DESCR, SRC_ID, SRC_SYS_ID, DEL_FLAG, INS_DT, INS_PROCESS_ID, UPD_DT, UPD_PROCESS_ID, UPD_EFF_DATE) VALUES (?, ?, ?, ?, ?, ?, TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), ?, TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), ?, TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'))\n")
               .setParameter(1, tpTestPartner.getTpPartnerKey())
               .setParameter(2, tpTestPartner.getId())
               .setParameter(3, tpTestPartner.getDescr())
               .setParameter(4, tpTestPartner.getSrcId())
               .setParameter(5, tpTestPartner.getSrcSysId())
               .setParameter(6, tpTestPartner.getDelFlag())
               .setParameter(7, tpTestPartner.getInsDt())
               .setParameter(8, tpTestPartner.getInsProcessId())
               .setParameter(9, tpTestPartner.getUpdDt())
               .setParameter(10, tpTestPartner.getUpdProcessId())
               .setParameter(11, tpTestPartner.getUpdEffDate())
               .executeUpdate();
    }

    @Transactional
    public void updateWithQuery(TpTestPartner tpTestPartner, int id) {
        entityManager.createNativeQuery("UPDATE \"ODS_API\".\"TP_TEST_PARTNER\" SET TP_PARTNER_KEY=?, ID=?, DESCR=?, SRC_ID=?, SRC_SYS_ID=?, DEL_FLAG=?, INS_DT=TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), INS_PROCESS_ID=?, UPD_DT=TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS'), UPD_PROCESS_ID=?, UPD_EFF_DATE=TO_DATE(?, 'YYYY-MM-DD HH24:MI:SS') WHERE TP_PARTNER_KEY = ?\n")
                .setParameter(1, tpTestPartner.getTpPartnerKey())
                .setParameter(2, tpTestPartner.getId())
                .setParameter(3, tpTestPartner.getDescr())
                .setParameter(4, tpTestPartner.getSrcId())
                .setParameter(5, tpTestPartner.getSrcSysId())
                .setParameter(6, tpTestPartner.getDelFlag())
                .setParameter(7, tpTestPartner.getInsDt())
                .setParameter(8, tpTestPartner.getInsProcessId())
                .setParameter(9, tpTestPartner.getUpdDt())
                .setParameter(10, tpTestPartner.getUpdProcessId())
                .setParameter(11, tpTestPartner.getUpdEffDate())
                .setParameter(12, id)
                .executeUpdate();
    }
}