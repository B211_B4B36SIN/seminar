package cz.cez.framework.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "TP_TEST_PARTNER")
public class TpTestPartner implements Serializable {

    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "TP_PARTNER_KEY")
    private int tpPartnerKey;

    @Column(name = "ID")
    private String id;

    @Column(name = "DESCR")
    private String descr;

    @Column(name = "SRC_ID")
    private String srcId;

    @Column(name = "SRC_SYS_ID")
    private String srcSysId;

    @Column(name = "DEL_FLAG")
    private int delFlag;

    //@DateTimeFormat(pattern = "dd.MM.yyyy")
    @Column(name = "INS_DT")
    private String insDt;

    @Column(name = "INS_PROCESS_ID")
    private String insProcessId;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @Column(name = "UPD_DT")
    private String updDt;

    @Column(name = "UPD_PROCESS_ID")
    private String updProcessId;

    @Column(name = "UPD_EFF_DATE")
    private String updEffDate;

    public TpTestPartner() {
    }

    public int getTpPartnerKey() {
        return tpPartnerKey;
    }

    public void setTpPartnerKey(int tpPartnerKey) {
        this.tpPartnerKey = tpPartnerKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getSrcId() {
        return srcId;
    }

    public void setSrcId(String srcId) {
        this.srcId = srcId;
    }

    public String getSrcSysId() {
        return srcSysId;
    }

    public void setSrcSysId(String srcSysId) {
        this.srcSysId = srcSysId;
    }

    public int getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(int delFlag) {
        this.delFlag = delFlag;
    }

    public String getInsDt() {
        return insDt;
    }

    public void setInsDt(String insDt) {
        this.insDt = insDt;
    }

    public String getInsProcessId() {
        return insProcessId;
    }

    public void setInsProcessId(String insProcessId) {
        this.insProcessId = insProcessId;
    }

    public String getUpdDt() {
        return updDt;
    }

    public void setUpdDt(String updDt) {
        this.updDt = updDt;
    }

    public String getUpdProcessId() {
        return updProcessId;
    }

    public void setUpdProcessId(String updProcessId) {
        this.updProcessId = updProcessId;
    }

    public String getUpdEffDate() {
        return updEffDate;
    }

    public void setUpdEffDate(String updEffDate) {
        this.updEffDate = updEffDate;
    }
}