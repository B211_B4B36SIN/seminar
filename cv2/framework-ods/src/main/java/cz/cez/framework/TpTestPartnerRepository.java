package cz.cez.framework;

import cz.cez.framework.model.TpTestPartner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TpTestPartnerRepository extends JpaRepository<TpTestPartner, Integer> {
}