package cz.cez.framework;

import cz.cez.framework.model.TestPartner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface TestPartnerRepository extends JpaRepository<TestPartner, Integer> {
    @Procedure(name = "GET_TEST_PARTNER_BY_COMP_NAME")
    List<TestPartner> getTestPartnerByCompName(@Param("COMP_NAME") String COMP_NAME);

    TestPartner getTestPartnerByFirstName(@Param("FIRSTNAME") String FIRSTNAME);
}