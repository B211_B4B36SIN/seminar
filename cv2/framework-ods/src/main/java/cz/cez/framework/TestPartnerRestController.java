package cz.cez.framework;

import cz.cez.framework.model.TestPartner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class TestPartnerRestController {

    @Autowired
    private TestPartnerRepository testPartnerRepository;

    @Autowired
    TestPartnerService testPartnerService;

    public void setTestPartnerRepository(TestPartnerRepository testPartnerRepository) {
        this.testPartnerRepository = testPartnerRepository;
    }


    @GetMapping("/api/testPartner")
    public List<TestPartner> getTPs() {
        List<TestPartner> testPartners = testPartnerRepository.findAll();
        return testPartners;
    }

    @GetMapping("/api/testPartner/{id}")
    public Optional<TestPartner> getTP(@PathVariable(name="id")Integer id) {
        return testPartnerRepository.findById(id);
    }

    @PostMapping("/api/testPartner")
    public ResponseEntity<?> saveTP(@RequestBody TestPartner testPartner){
        testPartnerService.insertWithQuery(testPartner);
        return ResponseEntity.ok("resource saved");
    }

    @DeleteMapping("/api/testPartner/{id}")
    public ResponseEntity<?> deleteTP(@PathVariable(name="id")Integer id){
        testPartnerRepository.deleteById(id);
        return ResponseEntity.ok("resource deleted");
    }

    @PutMapping("/api/testPartner/{id}")
    public ResponseEntity<?> updateTP(@RequestBody TestPartner testPartner,
                                      @PathVariable(name="id")Integer id){
        TestPartner ttp = testPartnerRepository.getOne(id);
        if(ttp != null){
            testPartnerService.updateWithQuery(testPartner,id);
        }
        return ResponseEntity.ok("resource updated");
    }
}