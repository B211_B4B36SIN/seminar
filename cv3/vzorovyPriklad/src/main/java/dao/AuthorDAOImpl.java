package dao;

import entity.Author;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by admin on 23/10/2017.
 */
public class AuthorDAOImpl extends GenericDAO<Author> implements AuthorDAO {

    public List<Author> findByLastName(String lastName) {
        TypedQuery<Author> query = em
                .createNamedQuery("findPersonByLastName", Author.class)
                .setParameter("name", lastName);
        return query.getResultList();
    }
}