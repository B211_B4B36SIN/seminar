package dao;

/**
 * Created by admin on 23/10/2017.
 */
import java.util.List;

interface DAO<T> {

    List<T> list();

    T save(T entity);

    void delete(Object id);

    T find(Object id);

    T update(T entity);
}