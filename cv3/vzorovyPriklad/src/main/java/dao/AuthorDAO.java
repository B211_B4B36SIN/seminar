package dao;

import entity.Author;

import java.util.List;

/**
 * Created by admin on 23/10/2017.
 */
public interface AuthorDAO extends DAO<Author> {

    List<Author> findByLastName(String lastName);

}