package service;

import entity.Author;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;

@Named("dummy")
@Stateless
public class Dummy {

    @Inject
    EntityManager em;

    public void trySomething(){
        System.out.println("Creating person");
        Author p = new Author();
        p.setEmail("neco@nekde.cz");
        p.setFirstName("John");
        p.setLastName("Roe");
        em.persist(p);
        em.flush();
    }
}