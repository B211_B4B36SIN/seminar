package cz.cvut.fel.demoapp.be.dto.response;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class Error {

    public static final String FIELD_MISSING = "FIELD_MISSING";
    public static final String FIELD_INVALID = "FIELD_INVALID";
    public static final String FIELD_TOO_LONG = "FIELD_TOO_LONG";
    public static final String FIELD_TOO_SHORT = "FIELD_TOO_SHORT";

    private String code;
    private String uuid;
    private String time;
    private Map<String, String> parameters;

    public Error parameter(String key, String value) {
        if (parameters == null) {
            parameters = new HashMap<>();
        }
        parameters.put(key, value);
        return this;
    }

    public void setErrorCode(ObjectError err) {
        String code = err.getCode();
        if (contains(code, NotEmpty.class, NotNull.class)) {
            setCode(FIELD_MISSING);
        } else if (contains(code, Size.class)) {
            validateSize(err);
        }
    }

    private void validateSize(ObjectError err) {
        int length = ((String) ((FieldError) err).getRejectedValue()).length();
        int minSize = (int) err.getArguments()[1];
        if (length > minSize) {
            setCode(FIELD_TOO_LONG);
        } else {
            setCode(FIELD_TOO_SHORT);
        }
    }

    private boolean contains(String name, Class<?>... c) {
        for (Class<?> cc : c) {
            if (cc.getSimpleName().equals(name)) {
                return true;
            }
        }
        return false;
    }

}
