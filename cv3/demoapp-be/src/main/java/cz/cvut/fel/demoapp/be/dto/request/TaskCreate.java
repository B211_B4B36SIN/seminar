package cz.cvut.fel.demoapp.be.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Getter
@Setter
public class TaskCreate {

    @NotEmpty
    @Size(max = 2000)
    private String text;
}
