package cz.cvut.fel.demoapp.be.dto.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Task {

    private long id;
    private String text;
    private CodeBookItem status;

}
