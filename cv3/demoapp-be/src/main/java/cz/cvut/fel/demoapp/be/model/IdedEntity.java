package cz.cvut.fel.demoapp.be.model;

import javax.persistence.*;
import java.util.Objects;

@MappedSuperclass
public abstract class IdedEntity {

    private static final String SEQUENCE_GENERATOR_NAME = "default_generator";
    private static final String SEQUENCE_NAME = "DEMOAPP_SEQUENCE";

    protected IdedEntity() {
    }

    protected IdedEntity(Long id) {
        this.id = id;
    }

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE_GENERATOR_NAME)
    @SequenceGenerator(name = SEQUENCE_GENERATOR_NAME, sequenceName = SEQUENCE_NAME, allocationSize = 1)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdedEntity that = (IdedEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "[" + id + "]";
    }

}
