package cz.cvut.fel.demoapp.be.model;

/**
 * The sole purpose of this class is to represent its package in order to allow for a type-safe component scanning.
 */
public class DemoAppDomainJpaPackage {

    private DemoAppDomainJpaPackage() {
        // Nothing to do here.
    }
}
