package cz.cvut.fel.demoapp.be.service;

import cz.cvut.fel.demoapp.be.model.Partner;
import cz.cvut.fel.demoapp.be.repository.PartnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PartnerDataService {

    @Autowired
    private PartnerRepository repository;

    public Partner getPartnerById(Long partnerId) {
        return repository.getById(partnerId);
    }

    public Partner getFirstByFirstNameOrLastNameOrDateOfBirth(String firstName, String lastName, String dateOfBirth) {
        return repository.getFirstByFirstNameOrLastNameOrDateOfBirth(firstName, lastName, dateOfBirth);
    }

}