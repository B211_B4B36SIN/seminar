package cz.cvut.fel.demoapp.be.service;

import cz.cvut.fel.demoapp.be.dto.request.TaskCreate;
import cz.cvut.fel.demoapp.be.dto.request.TaskUpdate;
import cz.cvut.fel.demoapp.be.dto.request.TaskUpdateStatus;

import cz.cvut.fel.demoapp.be.exception.FieldMissingException;
import cz.cvut.fel.demoapp.be.exception.NotFoundException;
import cz.cvut.fel.demoapp.be.model.CbStatus;
import cz.cvut.fel.demoapp.be.model.Task;
import cz.cvut.fel.demoapp.be.repository.TaskRepository;
import cz.cvut.fel.demoapp.be.util.CodeBookUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.isEmpty;

@Component
@Transactional
public class TaskService {

    @Autowired
    private TaskRepository repository;

    @Autowired
    private CodeBookMapper codeBookMapper;

    public TaskService(TaskRepository repository, CodeBookMapper codeBookMapper) {
        this.repository = repository;
        this.codeBookMapper = codeBookMapper;
    }

    public List<Task> getTasks(String partnerId) {
        validate(partnerId);
        return repository.findByPartnerIdOrderByCreatedDate(partnerId)
                .map(t -> {
                    Task dto = new Task();
                    dto.setId(t.getId());
                    dto.setText(t.getText());
                    return dto;
                })
                .collect(Collectors.toList());
    }

    public long create(String partnerId, TaskCreate dto) {
        cz.cvut.fel.demoapp.be.model.Task task = new cz.cvut.fel.demoapp.be.model.Task();
        task.setText(dto.getText());
        task.setPartnerId(partnerId);
        task.setStatus(CbStatus.ACTIVE);
        repository.save(task);
        return task.getId();
    }

    public void deleteCompletedTasks(String partnerId) {
        validate(partnerId);
        repository.deleteByPartnerIdAndStatus(partnerId, CbStatus.COMPLETED);
    }

    public void update(String partnerId, long taskId, TaskUpdate request) {
        validate(partnerId, taskId);
        repository.findByIdAndPartnerId(taskId, partnerId).setText(request.getText());
    }

    public void delete(String partnerId, long taskId) {
        validate(partnerId, taskId);
        repository.deleteById(taskId);
    }

    public void updateStatus(String partnerId, long taskId, TaskUpdateStatus request) {
        validate(partnerId, taskId);
        CbStatus status = CodeBookUtils.cb(CbStatus.class, request.getStatus().getCode());
        repository.findByIdAndPartnerId(taskId, partnerId).setStatus(status);
    }

    private void validate(String partnerId) {
        if (isEmpty(partnerId)) {
            throw new FieldMissingException();
        }
        if (!repository.existsByPartnerId(partnerId)) {
            throw new NotFoundException("PARTNER_NOT_FOUND");
        }
    }

    private void validate(String partnerId, long taskId) {
        validate(partnerId);
        if (!repository.existsByPartnerIdAndId(partnerId, taskId)) {
            throw new NotFoundException("TASK_NOT_FOUND");
        }
    }
}
