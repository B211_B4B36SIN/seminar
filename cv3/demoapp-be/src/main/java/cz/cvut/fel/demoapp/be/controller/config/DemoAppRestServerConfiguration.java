package cz.cvut.fel.demoapp.be.controller.config;


import cz.cvut.fel.demoapp.be.controller.DemoAppRestServerPackage;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan(basePackageClasses = DemoAppRestServerPackage.class)
public class DemoAppRestServerConfiguration {

}
