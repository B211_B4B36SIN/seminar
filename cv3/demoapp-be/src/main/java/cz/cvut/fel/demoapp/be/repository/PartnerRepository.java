package cz.cvut.fel.demoapp.be.repository;

import cz.cvut.fel.demoapp.be.model.Partner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface PartnerRepository extends CrudRepository<Partner, Long> {
    Partner getById(long Id);

    Partner getByFirstName(String name);

    Partner getFirstByFirstNameOrLastNameOrDateOfBirth(String firstName, String lastName, String dateOfBirth);
}
