create sequence TASK_S start with 1 increment by 1;

create table TASK (
    ID          number(10) unique not null,
    PARTNER_ID  varchar(50 char),
    STATUS_CODE varchar(50 char),
    TEXT        varchar(200 char)
);

insert into TASK (ID, PARTNER_ID, STATUS_CODE, TEXT)
values (TASK_S.nextval, '111', 'ACTIVE', 'Identify resources to be monitored');

insert into TASK (ID, PARTNER_ID, STATUS_CODE, TEXT)
values (TASK_S.nextval, '111', 'ACTIVE', 'Define users and workflow');

insert into TASK (ID, PARTNER_ID, STATUS_CODE, TEXT)
values (TASK_S.nextval, '111', 'ACTIVE', 'Identify event sources by resource type');

insert into TASK (ID, PARTNER_ID, STATUS_CODE, TEXT)
values (TASK_S.nextval, '222', 'COMPLETED', 'Define the relationship between resources and business systems');