package cz.cvut.fel.demoapp.be.controller.controller;

import cz.cez.cpr.demoapp.rest.server.config.DemoAppRestServerTestConfiguration;
import cz.cez.cpr.demoapp.rest.server.config.RestResponseEntityExceptionHandler;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ContextConfiguration(
		classes = {
				DemoAppRestServerTestConfiguration.class,
				RestResponseEntityExceptionHandler.class
		}
)
public abstract class AbstractControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	protected MockMvc mockMvc;

	@BeforeEach
	void beforeEach() {
		if (mockMvc == null) {
			mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		}
	}
}
