package cz.cvut.fel.demoapp.be.service;


import cz.cvut.fel.demoapp.be.dto.response.CodeBookItem;
import cz.cvut.fel.demoapp.be.exception.FieldMissingException;
import cz.cvut.fel.demoapp.be.exception.NotFoundException;
import cz.cvut.fel.demoapp.be.model.CbStatus;
import cz.cvut.fel.demoapp.be.model.Task;
import cz.cvut.fel.demoapp.be.repository.TaskRepository;
import cz.cvut.fel.demoapp.be.service.CodeBookMapper;
import cz.cvut.fel.demoapp.be.service.TaskService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class TaskServiceTest {

	public static final String PARTNER_ID = "123456789";

	@Mock
	private TaskRepository repository;
	private final CodeBookMapper codeBookMapper = new CodeBookMapper();
	private TaskService service;

	@BeforeEach
	public void setUp() {
		mock(task(1L, CbStatus.COMPLETED, "Task 1"),
				task(2L, CbStatus.ACTIVE, "Task 2"));
		this.service = new TaskService(repository, codeBookMapper);
	}

	private void mock(Task... tasks) {
		lenient().when(repository.existsByPartnerId(anyString()))
				.thenAnswer(i -> PARTNER_ID.equals(i.getArgument(0)));
		lenient().when(repository.findByPartnerIdOrderByCreatedDate(anyString()))
				.thenAnswer(i -> PARTNER_ID.equals(i.getArgument(0)) ? Stream.of(tasks) : Stream.empty());
	}

	@Test
	public void findByPartnerId() {
		List<Task> tasks = service.getTasks(PARTNER_ID);
		verify(repository).existsByPartnerId(PARTNER_ID);
		verify(repository).findByPartnerIdOrderByCreatedDate(PARTNER_ID);
		assertSame(2, tasks.size());
		validate(tasks.get(0), 1L, codeBookMapper.map(CbStatus.COMPLETED), "Task 1");
		validate(tasks.get(1), 2L, codeBookMapper.map(CbStatus.ACTIVE), "Task 2");
	}

	@Test
	public void findByPartnerIdNotFound() {
		assertThrows(NotFoundException.class, () -> service.getTasks("777"));
	}

	@Test
	public void findByPartnerIdFieldMissing() {
		assertThrows(FieldMissingException.class, () -> service.getTasks(""));
	}

	private Task task(Long id, CbStatus status, String text) {
		Task task = new Task();
		task.setId(id);
		task.setPartnerId(PARTNER_ID);
		task.setStatus(status);
		task.setText(text);
		return task;
	}

	private void validate(Task task, Long id, CodeBookItem cb, String text) {
		assertSame(id, task.getId());
		assertSame(text, task.getText());
		assertNull(task.getStatus());
	}
}
