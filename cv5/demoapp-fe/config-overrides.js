const path = require("path");
const rewireReactHotLoader = require("react-app-rewire-hot-loader");

module.exports = function override(config, env) {
	config = rewireReactHotLoader(config, env);

	config.resolve = {
		...config.resolve,
		alias: {
			"@core": path.resolve(__dirname, "src", "core", "index.tsx"),
			"@layout": path.resolve(__dirname, "src", "layout", "index.tsx"),
			"@constants": path.resolve(__dirname, "src", "constants.tsx"),
			"@library": path.resolve(__dirname, "src", "library", "index.tsx"),
			"@theme": path.resolve(__dirname, "src", "theme", "index.tsx"),
			"@store": path.resolve(__dirname, "src", "store", "index.tsx"),
			"@components": path.resolve(__dirname, "src", "components", "index.tsx"),
			"@api": path.resolve(__dirname, "src", "api", "index.tsx"),
			"@localization": path.resolve(__dirname, "src", "localization", "index.tsx")
		},
	};

	return config;
};
