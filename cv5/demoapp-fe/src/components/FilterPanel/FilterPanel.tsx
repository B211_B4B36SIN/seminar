import React, { useState } from "react";
import { CEZFilterPanelProps, CEZFilterBodyItem, PartnerTaskItem } from "@library";
import { Button } from "@components";
import { Select, TextField, MenuItem, InputLabel, FormControl, Container, Typography } from "@material-ui/core";
import { useContainerStyles, useHeaderStyles } from "./styles";
import { useLocalization } from "@localization";
import langFile from "./translations.json";

const FilterPanel = ({ options, onFilter }: CEZFilterPanelProps<PartnerTaskItem>) => {

    const translations = useLocalization(langFile);
    const [filterValue, setFilterValue] = useState<string>();
    const [filterType, setFilterType] = useState<any>(0);

    const handleChange = (e: React.ChangeEvent<{name?: string, value: any}>) => {
        setFilterType("");
    };

    return (
        <Container classes={useContainerStyles()}>
            <Typography 
                data-test-id="filter-panel-header"
                classes={useHeaderStyles()}
            >{translations.filterPanelHeader}</Typography>
            <TextField 
                label={translations.filterInputLabel}
                variant="outlined"
                aria-label="filter-input-field"
                data-test-id="filter-input-field"
                value={filterValue}
                onChange={e => setFilterValue(e.target.value)}
            />
            <FormControl>
                <InputLabel shrink id="filter-by-label" data-test-id="filter-by-label">
                    {translations.selectLabel}
                </InputLabel>
                <Select
                    labelId="filter-by-label"
                    id="filter-by-select"
                    data-test-id="filter-by-select"
                    aria-label="partner filter-by select box"
                    value={filterType}
                    onChange={handleChange}
                    displayEmpty
                >
                    {options.map((option: CEZFilterBodyItem<PartnerTaskItem>, index: number) => (
                        <MenuItem 
                            key={`filter-panel-key-${index}`} 
                            value={option.value.text}
                        >
                            {option.text}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
           
            <Button onClick={() => onFilter(filterValue, filterType)}>{translations.filterButton}</Button>
        </Container>
    );
};

export default FilterPanel;
