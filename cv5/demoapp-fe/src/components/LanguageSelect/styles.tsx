import { makeStyles, Theme } from "@material-ui/core";

const usePopperStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: 0,
        margin: "1rem"
    }
}));

const useLangButtonStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: 0,
        float: "right"
    },
    endIcon: {
        marginRight: "0.5rem"
    },
    label: {
        justifyContent: "flex-end"
    }
}));

const useLangButtonIconStyles = makeStyles((theme: Theme) => ({
    root: {
        width: "2rem",
        height: "2rem"
    }
}));

export {
    usePopperStyles,
    useLangButtonStyles,
    useLangButtonIconStyles
};
