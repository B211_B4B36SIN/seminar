import React from "react";
import { Button as MUIButton, ButtonProps } from "@material-ui/core";

const Button = (props: ButtonProps) => {

    return (
        <MUIButton {...props}>
            {props.children}
        </MUIButton>
    );
};

export default Button;
