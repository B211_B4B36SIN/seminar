import React from "react";
import useStyles from "./styles";
import { Modal, CircularProgress } from "@material-ui/core";
import { ModalProgressProps } from "@library";

const ModalProgress = (props: ModalProgressProps) => {

    const styles = useStyles();

    return (
        <Modal
            className={styles.modal}
            open={props.open}
            onClose={props.onClose}
            aria-labelledby="loading"
            aria-describedby={"loading"}
        >
            <CircularProgress className={styles.progress} />
        </Modal>
    )
};

export default ModalProgress;
