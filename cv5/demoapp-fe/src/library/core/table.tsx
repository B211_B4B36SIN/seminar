import { TableProps } from "@material-ui/core";



export type CEZTableProps = TableProps & {
    items: any[],
    titles?: CEZTableHeadItem[],
    emptyTableMessage?: string,
    mapItemsToTitleIds?: boolean
};



export type CEZTableHeadItem = {
    id: string,
    value: string,
    numeric?: boolean,
    disablePadding?: boolean,
    unsortable?: boolean
};
