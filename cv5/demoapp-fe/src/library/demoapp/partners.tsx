export type PartnerDataItem = {
    id: number,
    firstName: string,
    lastName: string,
    dateOfBirth: string,
    personNumber: number
};

export type PartnerDetailsProps = {
    partner: PartnerDataItem
};

export enum EPartnerFilter {
    ID,
    NAME_SURNAME_DOB
};

export type PartnerTasksListProps = {
    partner: PartnerDataItem
};
