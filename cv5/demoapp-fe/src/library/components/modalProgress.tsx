export type ModalProgressProps = { 
    open: boolean,
    onClose?: () => void
};
