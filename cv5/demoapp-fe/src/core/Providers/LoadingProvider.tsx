import React, { useState } from "react";
import { LoadingContext } from "@core";
import { ModalProgress } from "@components";

const LoadingProvider = ({ children }: any) => {

    const [loading, setLoading] = useState<boolean>(false);

    return (
        <LoadingContext.Provider value={{ inProgress: loading, set: setLoading }}>
            {children}
            <ModalProgress open={loading} />
        </LoadingContext.Provider>
    );
};

export default LoadingProvider;
