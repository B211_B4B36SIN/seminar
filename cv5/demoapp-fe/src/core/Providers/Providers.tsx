import React from "react";
import LanguageProvider from "./LanguageProvider";
import LoadingProvider from "./LoadingProvider";
import UserProvider from "./UserProvider";

const Providers = ({ children }: any) => {
    return (
        <LanguageProvider>
            <LoadingProvider>
                <UserProvider>
                    {children}
                </UserProvider>
            </LoadingProvider>
        </LanguageProvider>
    )
};

export default Providers;
