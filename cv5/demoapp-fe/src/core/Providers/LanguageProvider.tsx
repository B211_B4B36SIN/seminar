import React, { useState } from "react";
import { ELocalizationLanguage } from "@library";
import { LOCAL_STORAGE_LOCALIZATION_KEY } from "@constants";
import { LanguageContext } from "@core";

const LanguageProvider = ({ children }: any) => {

    const [lang, setLang] = useState(ELocalizationLanguage.CZ);

    const onChangeLocalization = (newLoc: ELocalizationLanguage) => {
        localStorage.setItem(LOCAL_STORAGE_LOCALIZATION_KEY, newLoc);
        setLang(newLoc as ELocalizationLanguage);
    };

    return (
        <LanguageContext.Provider value={{ language: lang, setLanguage: onChangeLocalization }}>
            {children}
        </LanguageContext.Provider>
    );
};

export default LanguageProvider;
