import React, { useState } from "react";
import { UserContext } from "@core";

const UserProvider = ({ children }: any) => {

    const [name] = useState<string>("Jim");
    const [surname] = useState<string>("Parsons");

    const userContextItem = {
        name,
        surname,
        isLoggedIn: true,
        redirectToLogin: false
    };

    return (
        <UserContext.Provider value={userContextItem}>
            {children}
        </UserContext.Provider>
    );
};

export default UserProvider;
