import colors from "./colors";

export default {
    primary: {
        main: colors.primary,
    },
    secondary: {
        main: colors.secondary,
    },
    error: {
        main: colors.error,
    },
    background: {
        default: colors.background,
    }
};
