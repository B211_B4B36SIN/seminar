import { createMuiTheme } from "@material-ui/core/styles";
import palette from "./palette";
import * as MuiButtonOverride from "./muiOverrides/button";
import MuiButtonGroupOverride from "./muiOverrides/buttonGroup";
import MuiPaperOverride from "./muiOverrides/paper";
import MuiChipOverride from "./muiOverrides/chip";
import * as MuiTableOverride from "./muiOverrides/table";
import MuiSvgIconOverride from "./muiOverrides/svgIcon"
import colors from "./colors";

const theme = createMuiTheme({
	palette: palette,
	typography: {
		htmlFontSize: 1.6,
		allVariants: {
			fontFamily: "NimbusCEZ",
			fontStyle: "normal",
			fontWeight: "normal",
			fontSize: "1.6rem",
			lineHeight: "2rem",
		},
		h1: {
			fontFamily: "FuturaCEZ",
			fontStyle: "normal",
			fontWeight: 600,
			fontSize: "4.8rem",
			lineHeight: "5.8rem",
			textTransform: "uppercase",
		},
		h2: {
			fontFamily: "FuturaCEZ",
			fontStyle: "normal",
			fontWeight: 600,
			fontSize: "3.2rem",
			lineHeight: "4rem",
			textTransform: "uppercase",
		},
		h3: {
			fontFamily: "FuturaCEZ",
			fontStyle: "normal",
			fontWeight: 600,
			fontSize: "2.4rem",
			lineHeight: "3.2rem",
			textTransform: "uppercase",
		},
		h4: {
			fontStyle: "normal",
			fontWeight: 600,
			fontSize: "1.6rem",
			lineHeight: "2.4rem",
			textTransform: "uppercase",
		},
		button: {
			fontFamily: "FuturaCEZ",
			fontStyle: "normal",
			fontWeight: 500,
			fontSize: "1.6rem",
			lineHeight: "1.6rem",
			textTransform: "uppercase",
		}
	},
	spacing: factor => `${factor}rem`,
	overrides: {
		MuiButton: MuiButtonOverride.button,
		MuiButtonGroup: MuiButtonGroupOverride,
		MuiIconButton: MuiButtonOverride.iconButton,
		MuiPaper: MuiPaperOverride,
		MuiChip: MuiChipOverride,
		MuiTableCell: MuiTableOverride.cell,
		MuiTableRow: MuiTableOverride.row,
		MuiSvgIcon: MuiSvgIconOverride
	}
});

export {
	theme,
	colors
};
