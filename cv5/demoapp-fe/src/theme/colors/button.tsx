import baseColors from "./baseColors";

export default {
    contained: {
        primary: {
            default: {
                text: baseColors.white,
                background: {
                    from: baseColors.lightOrange,
                    to: baseColors.orange
                }
            },
            hover: {
                background: {
                    from: baseColors.lightOrange,
                    to: baseColors.orange
                }
            },
            disabled: {
                background: baseColors.grey4,
            }
        },
        secondary: {
            background: baseColors.grey4
        }
    },
    text: {
        default: {
            text: baseColors.orange,
            background: baseColors.transparent
        },
        hover: {
            background: baseColors.orange12
        },
        disabled: {
            text: baseColors.grey3,
            border: baseColors.grey4,
            background: baseColors.transparent
        }
       
    },
    outlined: {
        default: { 
            text: baseColors.orange,
            border: baseColors.orange36,
            background: baseColors.transparent
        },
        hover: {
            bacgkround: baseColors.orange12,
            border: baseColors.orange12
        },
        active: {
            background: baseColors.orange36
        },
        disabled: {
            text: baseColors.grey3,
            border: baseColors.grey4,
            background: baseColors.transparent
        }
    }
};
