import baseColors from "./baseColors";
import buttonColors from "./button";
import tableColors from "./table";
import mainMenuColors from "./mainMenuColors";
import appColors from "./appColors";

export default {
    primary: baseColors.orange,
    secondary: baseColors.white,
    error: baseColors.red,
    background: baseColors.transparent,
    text: {
        primary: baseColors.orange,
        secondary: baseColors.white
    },
    borders1: baseColors.grey4,
    button: buttonColors,
    table: tableColors,
    mainMenu: mainMenuColors,
    app: appColors
}
