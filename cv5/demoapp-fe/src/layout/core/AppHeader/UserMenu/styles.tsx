import { makeStyles, Theme } from "@material-ui/core";

const usePopperStyles = makeStyles((theme: Theme) => ({
    root: {
        padding: 0,
        margin: "1rem"
    }
}));

export {
    usePopperStyles
};
