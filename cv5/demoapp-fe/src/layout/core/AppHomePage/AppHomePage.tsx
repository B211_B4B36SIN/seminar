import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { Typography, Grid } from "@material-ui/core";
import { LibraryBooks, RecentActors } from "@material-ui/icons";
import { Button } from "@components";
import { setPartners } from "@store";
import { DEMOAPP_URLS, COMPONENTS_LIBRARY_URLS } from "@constants";
import { useLocalization } from "@localization";
import useStyles, { useMenuButtonStyles } from "./styles";
import transFile from "./translations.json";

const AppHomePage = () => {

    const styles = useStyles();
    const menuButtonStyles = useMenuButtonStyles();
    const translations = useLocalization(transFile);
    const dispatch = useDispatch();
    
    return (
        <>
            <Typography 
                align="center"
                variant="h2"
                color="primary" 
                className={styles.header}
            >
                {translations.title}
            </Typography>
            
            <Grid container justify="center" spacing={2}>
                <Grid item>
                    <Link to={COMPONENTS_LIBRARY_URLS.main}>
                        <Button
                            classes={menuButtonStyles}
                            aria-label="Go to components library"
                            data-test-id="app-settings"
                            variant="outlined"
                            startIcon={<LibraryBooks />}
                        >{translations.componentsLibraryLabel}</Button>
                    </Link>
                </Grid>
                <Grid item>
                    <Link to={DEMOAPP_URLS.partnersList}>
                        <Button
                            classes={menuButtonStyles}
                            aria-label="test"
                            data-test-id="test-id"
                            variant="outlined"
                            startIcon={<RecentActors />}
                            onClick={() => dispatch(setPartners([]))}
                        >{translations.demoAppLabel}</Button>
                    </Link>
                </Grid>
            </Grid>
        </>
    );
};

export default AppHomePage;
