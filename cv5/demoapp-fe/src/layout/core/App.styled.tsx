import styled from "styled-components";
import { colors } from "@theme";

const AppStyled = styled.article`
    display: flex;
    flex-direction: column;
    min-height: 100vh;

    & .app-section {
        margin: 0 auto;
        width: 76.8rem;
        padding: 2rem;

        @media only screen and (max-width: 768px) {
            width: 100%;
        }
       
    }
    
    & .app-footer {
        padding: 1rem 0;
        margin-top: auto;
        border-top: 0.1rem solid ${colors.borders1};
        text-align: center;
    }
`;

export default AppStyled;
