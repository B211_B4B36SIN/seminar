import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Button } from "@components";
import { List, ListItem, Link, Typography, TextField, Modal, IconButton, Paper } from "@material-ui/core";
import { Delete } from "@material-ui/icons";
import { PartnerTaskItem, PartnerTasksListProps } from "@library";
import { useLocalization } from "@localization";
import useStyles from "./styles";
import useTasksList from "./useTasksList";
import useTaskModalControls from "./useTaskModalControls";
import transFile from "./translations.json";
import { DEMOAPP_URLS } from "@constants";

const TasksList = ({ partner }: PartnerTasksListProps) => {

    const tasks = useTasksList();
    const modalWindow = useTaskModalControls();
    const styles = useStyles();
    const history = useHistory();
    const translations = useLocalization(transFile);

    useEffect(() => {

        if (partner.id === -1) {
            history.push(DEMOAPP_URLS.partnersList);
            return;
        }

        tasks.onPartnerUpdated(partner.id);
        modalWindow.onPartnerUpdated(partner.id);

    }, [partner]);

    return (
        <>
            <Typography variant="h2" color="primary" align="center">{translations.tasksTitle}</Typography>

            <List>
                {tasks.items.map((task: PartnerTaskItem, index: number) => (
                    <ListItem
                        component={Paper}
                        className={styles.tasksListItem} 
                        key={`task-${index}`}
                    >
                        <Link 
                            className={styles.tasksListLabel}
                            onClick={() => modalWindow.openEditTask(task)}
                        >{task.text}</Link>

                        <Button
                            data-test-id="toggle-task-completed-button"
                            variant="outlined"
                            color="primary"
                            className={styles.finishTaskButton}
                            onClick={(e) => tasks.toggleTaskCompleted(task.id)}
                        >
                            {task.status.code === "ACTIVE" ? 
                                translations.completeTaskButton : translations.openTaskButton
                            }
                        </Button>

                        <IconButton 
                            color="primary" 
                            data-test-id="delete-task-button" 
                            aria-label="delete task" 
                            component="span"
                            onClick={() => tasks.deleteTask(task.id)}
                        >
                            <Delete />
                        </IconButton>

                    </ListItem>
                ))}
            </List>

            <Button
                aria-label="Open modal for creating a new task"
                data-test-id="create-new-task-button"
                onClick={modalWindow.openNewTask}
                variant="contained"
                className={styles.bottomBarButton}
            >{translations.createNewTaskButton}</Button>

            <Button
                color="primary"
                endIcon={<Delete/>}
                className={styles.bottomBarButton}
                disabled={!tasks.hasFinishedTasks}
                onClick={tasks.deleteCompletedTasks}
            >
                {translations.deleteCompletedTasksLabel}
            </Button>

            <Modal
                className={styles.modal}
                open={modalWindow.isModalOpen}
                onClose={modalWindow.close}
                aria-labelledby="add new task"
                aria-describedby={translations.modalWindowDescription}
            >
                <Paper>
                    {modalWindow.editTaskWindowOpen && (<>
                        <TextField
                            id="modal-task-edit-name"
                            data-test-id="modal-task-edit-name"
                            aria-label="Edit task name"
                            error={modalWindow.invalidTaskName}
                            helperText={modalWindow.invalidTaskName ? translations.errorTaskNameTooShort : ""}
                            value={modalWindow.task.text}
                            onChange={e => modalWindow.setTaskText(e.target.value)}
                        />
                        <Button
                            data-test-id="edit task button"
                            aria-label="Confirm new task name"
                            disabled={modalWindow.invalidTaskName}
                            onClick={() => modalWindow.confirmEditTask(task => tasks.onTaskEdited(task))}
                        >{translations.confirmTaskEditButton}</Button>
                    </>)}
                    {modalWindow.newTaskWindowOpen && (<>
                        <Typography 
                            className={styles.newTaskTitle}
                            align="center"
                            variant="h4"
                        >{translations.newTaskModalTitle}</Typography>
                        <TextField 
                            label={translations.newTaskNameLabel}
                            variant="outlined"
                            size="small"
                            aria-label="New task name"
                            data-test-id="new-task-name-input"
                            error={modalWindow.invalidTaskName}
                            helperText={modalWindow.invalidTaskName ? translations.errorTaskNameTooShort : ""}
                            id="new-task-name=id" 
                            value={modalWindow.task.text}
                            onChange={e => modalWindow.setTaskText(e.target.value)}
                        />
                        <Button
                            variant="outlined"
                            color="primary"
                            className={styles.confirmAddTaskButton}
                            disabled={modalWindow.invalidTaskName}
                            onClick={() => {
                                tasks.addNewTask(modalWindow.task.text);
                                modalWindow.close();
                            }}
                        >{translations.addTaskButton}</Button>
                    </>)}
                </Paper>
            </Modal>
        </>
    );
};

export default TasksList;
