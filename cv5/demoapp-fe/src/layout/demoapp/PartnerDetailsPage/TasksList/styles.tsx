import { makeStyles, Theme, createStyles } from "@material-ui/core";


const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        modal: {
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
        },
        newTaskTitle: {
            paddingBottom: "1rem"
        },
        confirmAddTaskButton: {
            display: "block",
            margin: "1rem auto 0 auto"
        },
        tasksListItem: {
            display: "flex",
        },
        tasksListLabel: {
            flex: 1,
            cursor: "pointer",
            textDecoration: "underline"
        },
        finishTaskButton: {
            height: "2rem",
            "&:last-child": {
                float: "right"
            }
        },
        bottomBarButton: {
            "&:last-child": {
                float: "right"
            }
        }
    })
);

export default useStyles;
