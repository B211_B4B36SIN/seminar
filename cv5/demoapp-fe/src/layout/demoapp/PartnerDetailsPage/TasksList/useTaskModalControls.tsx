import { useState, useContext } from "react";
import * as api from "@api";
import { PartnerTaskItem } from "@library";
import { LoadingContext } from "@core";

const useTaskModalControls = () => {

    const defaultTask: PartnerTaskItem = {
        id: -1,
        text: "New task",
        status: {
            code: "ACTIVE",
            description: "a new Task",
            order: -1,
            attributes: {}
        }
    };

    const [task, setTask] = useState<PartnerTaskItem>(defaultTask);
    const [invalidTaskName, setInvalidTaskName] = useState<boolean>(false);
    const [newTaskWindowOpen, setNewTaskWindowOpen] = useState<boolean>(false);
    const [editTaskWindowOpen, setEditTaskWindowOpen] = useState<boolean>(false);
    const [partnerId, setPartnerId] = useState<number>();
    const loading = useContext(LoadingContext);

    const setTaskText = (newText: string) => {
        
        setInvalidTaskName(newText.length < 1);
        setTask({
            ...task,
            text: newText
        });
    }

    const onPartnerUpdated = (partnerId: number) => {
        setPartnerId(partnerId);
    };

    const confirmEditTask = (callback?: (newTask: PartnerTaskItem) => void) => {
        loading.set(true);

        api.updateTaskName(partnerId, task.id, task.text)
        .then(response => {
            close();
            if (callback) callback(task);
        });
    };

    const openNewTask = () => {
        setTask(defaultTask);
        setNewTaskWindowOpen(true);
        setEditTaskWindowOpen(false);
    };

    const openEditTask = (task: PartnerTaskItem) => {
        setTask(task);
        setNewTaskWindowOpen(false);
        setEditTaskWindowOpen(true);
    };

    const close = () => {
        loading.set(false);
        setNewTaskWindowOpen(false);
        setEditTaskWindowOpen(false);
    };

    return {
        onPartnerUpdated,
        task,
        confirmEditTask,
        close,
        openNewTask,
        openEditTask,
        isModalOpen: newTaskWindowOpen || editTaskWindowOpen,
        newTaskWindowOpen,
        editTaskWindowOpen,
        invalidTaskName,
        setTaskText
    };

};

export default useTaskModalControls;
