import { makeStyles, Theme, createStyles } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => 
    createStyles({
        filterForm: {
            width: "20rem"
        },
        filterInput: {
            padding: "2rem 0",
            "& .MuiTextField-root": {
                marginRight: "1rem"
            }
        }
    })
);

export default useStyles;
