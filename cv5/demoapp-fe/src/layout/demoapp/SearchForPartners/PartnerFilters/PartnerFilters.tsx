import React, { useContext } from "react";
import { FormControl, InputLabel, Select, MenuItem, TextField, Typography, Container, Paper } from "@material-ui/core";
import { useLocalization } from "@localization";
import { EPartnerFilter } from "@library";
import { LoadingContext } from "@core";
import { ModalProgress, Button } from "@components";
import useFilters from "./useFilters";
import useStyles from "./styles";
import transFile from "./translations.json";

const PartnerFilters = () => {

    const translations = useLocalization(transFile);
    const loading = useContext(LoadingContext);
    const filters = useFilters();
    const styles = useStyles();

    return (
        <Paper>
            <FormControl className={styles.filterForm} >
                <InputLabel id="filter-type-select-label">{translations.filterTypeLabel}</InputLabel>
                <Select
                    labelId="filter-type-select-label"
                    id="filter-type-select"
                    data-test-id="filter-type-select"
                    aria-label="filter type selector"
                    value={filters.activeFilter}
                    onChange={filters.handleFilterChange}
                >
                    <MenuItem value={EPartnerFilter.NAME_SURNAME_DOB}>{translations.filterSurnameLabel}</MenuItem>
                    <MenuItem value={EPartnerFilter.ID}>{translations.filterIdLabel}</MenuItem>
                </Select>
                
            </FormControl>

            <Container className={styles.filterInput}>
                {filters.activeFilter === EPartnerFilter.ID && (
                    <TextField 
                        data-test-id="filter-id"
                        aria-label="filter id input"
                        value={filters.id}
                        type="number"
                        onChange={e => filters.set("id", e.target.value)}
                        error={filters.invalidIdTooLong} 
                        helperText={filters.invalidIdTooLong ? translations.invalidIdTooLong : ""}
                        id="filter-by-id-id" 
                        label={translations.filterInputIdLabel} 
                    />
                )}
                {filters.activeFilter === EPartnerFilter.NAME_SURNAME_DOB && (
                    <>
                        <TextField 
                            data-test-id="filter-name"
                            aria-label="filter name input"
                            value={filters.name}
                            onChange={e => filters.set("name", e.target.value)}
                            error={filters.invalidNameTooLong} 
                            helperText={filters.invalidNameTooLong ? translations.invalidNameTooLong : ""}
                            id="filter-name-id" 
                            label={translations.filterInputNameLabel} 
                        />
                        <TextField 
                            data-test-id="filter-surname"
                            aria-label="filter surname input"
                            value={filters.surname}
                            onChange={e => filters.set("surname", e.target.value)}
                            error={filters.invalidSurnameTooLong} 
                            helperText={filters.invalidSurnameTooLong ? translations.invalidSurnameTooLong : ""}
                            id="filter-surname-id"
                            label={translations.filterInputSurnameLabel}
                        />
                        <TextField
                            id="date-picker"
                            label={translations.datePickerLabel}
                            type="date"
                            value={filters.dateOfBirth}
                            onChange={(e) => filters.set("dateOfBirth", e.target.value)}
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </>
                )}
            </Container>
        
            {filters.filterIsInvalid && (
                filters.activeFilter === EPartnerFilter.ID ? (
                    <Typography
                        color="error"
                        data-test-id="invalid-filter-id"
                    >{translations.invalidFilterId}</Typography>
                ) : (
                    <Typography 
                        color="error"
                        data-test-id="invalid-filter-name-surname-dob"
                    >{translations.invalidNameSurnameDob}</Typography>
                )
            )}

            <Button 
                variant="contained"
                color="primary"
                aria-label="apply filters"
                data-test-id="apply-filters-button"
                disabled={filters.filterIsInvalid}
                onClick={filters.handleApplyFilters}
            >{translations.filterButton}</Button>

            <ModalProgress open={loading.inProgress} />
            
        </Paper>
    );
};

export default PartnerFilters;
