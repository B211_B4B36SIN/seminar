import React from 'react';
import ReactDOM from 'react-dom';
import App from "./layout/core/App";
import "./index.css";
import store from "@store";
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import { theme } from "@theme";
import { ThemeProvider } from "@material-ui/core";

document.title = "DemoApp frontend";

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <Provider store={store}>
      <App />
    </Provider>
  </ThemeProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
