import axios from "axios";
import { PartnerDataItem } from "@library";
import { JDZ_BASE_URL, TOKEN } from "./constants";

const baseUrl = `${JDZ_BASE_URL}/partners`;

const getFilteredPartners = (name: string, surname: string, dateOfBirth: string) => {

    let params: any = {};

    if (name) params["firstName"] = name;
    if (surname) params["lastName"] = surname;
    if (dateOfBirth) params["dateOfBirth"] = dateOfBirth;
    
    const config = { 
        params,
        headers: {
            Authorization: TOKEN 
        } 
    };

    return axios.get<PartnerDataItem[]>(baseUrl, config);
};

const getPartnerById = (id: number) => {

    const config = { 
        headers: {
            Authorization: TOKEN 
        }
    };

    return axios.get<PartnerDataItem>(`${baseUrl}/${id}`, config);
}

export {
    getFilteredPartners,
    getPartnerById
};
