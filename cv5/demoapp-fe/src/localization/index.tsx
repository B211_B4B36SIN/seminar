import { useContext } from "react";
import { ELocalizationLanguage } from "@library";
import { LanguageContext } from "@core";

const useLocalization = (languageFile: any) => {

    const languageContext = useContext(LanguageContext);

    switch(languageContext.language) {
        case ELocalizationLanguage.CZ: return languageFile.cz;
        case ELocalizationLanguage.ENGB: return languageFile.engb;
        default: return languageFile.cz; 
    }
};

export {
    useLocalization
};
