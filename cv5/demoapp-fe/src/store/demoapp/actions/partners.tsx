import { PartnerDataItem } from "@library";
import * as actions from "../actionsLibrary";

const setPartners = (items: PartnerDataItem[]) => ({
    type: actions.SET_PARTNERS_LIST,
    payload: items
});

export {
    setPartners
};
