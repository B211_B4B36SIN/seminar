// @ts-nocheck
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";
import demoappRootReducer from "./demoapp/reducers/rootReducer";

const rootReducer = combineReducers({
    demoapp: demoappRootReducer,
});

const store = !window.__REDUX_DEVTOOLS_EXTENSION__ ?  
    createStore(
        rootReducer, 
        applyMiddleware(thunk),   
    ) : 
    createStore(
        rootReducer, 
        compose(
            applyMiddleware(thunk),   
            window.__REDUX_DEVTOOLS_EXTENSION__()
        )
    );

export * from "./demoapp/actions";

export default store;