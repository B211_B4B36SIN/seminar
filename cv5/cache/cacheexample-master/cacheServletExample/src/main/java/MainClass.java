import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.HazelcastClient;
import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.IdGenerator;

import java.util.Map;


public class MainClass {
    public static void main(String[] args) {
//distributed map..
    /**/
        Config cfg = new Config();
        HazelcastInstance instance = Hazelcast.newHazelcastInstance(cfg);
        Map<Integer, String> mapCustomers = instance.getMap("customers");
        mapCustomers.put(1, "Joe");// 1 users // 2 roles // 3
        mapCustomers.put(2, "Ali");
        mapCustomers.put(3, "Avi");

        System.out.println("Customer with key 1: "+ mapCustomers.get(1));
        System.out.println("Map Size:" + mapCustomers.size());

        HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance();

        Map<Long, String> map = hazelcastInstance.getMap("data");

        IdGenerator idGenerator = hazelcastInstance.getIdGenerator("newid");
        for (int i = 0; i < 10; i++) {
            map.put(idGenerator.newId(), "message" + 1);
        }

        IMap<Long, String> map2 = hazelcastInstance.getMap("data");
        System.out.println("vypis");
        for (Map.Entry<Long, String> entry : map2.entrySet()) {
             System.out.println(entry.getValue());
        }
/*
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.getNetworkConfig().addAddress("127.0.0.171:5701");//getNetworkConfig 5701 , 171 na konci
        HazelcastInstance client = HazelcastClient.newHazelcastClient(clientConfig);
        IMap map = client.getMap("objects");
        System.out.println("Map Size:" + map.size());*/



    }
}
