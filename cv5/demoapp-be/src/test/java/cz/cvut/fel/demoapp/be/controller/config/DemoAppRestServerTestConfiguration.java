package cz.cvut.fel.demoapp.be.controller.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class DemoAppRestServerTestConfiguration implements WebMvcConfigurer {
}
