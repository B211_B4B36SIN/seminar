package cz.cvut.fel.demoapp.be.controller.controller.pub;

import cz.cvut.fel.demoapp.be.controller.api.TaskController;
import cz.cvut.fel.demoapp.be.controller.controller.AbstractControllerTest;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest
@ContextConfiguration(
		classes = {
				TaskController.class
		}
)
public class TaskControllerTest extends AbstractControllerTest {

	private static final Logger log = LoggerFactory.getLogger(TaskControllerTest.class);

	@MockBean
	private TaskFacade taskFacade;

	@Test
	public void test() throws Exception {

		String partnerId = "111";

		when(taskFacade.getTask(eq(partnerId))) //service
				.thenReturn(Stream.of(
						new Task().id(1L).text("t1").status(new CodeBookItem().code("ACTIVE")),
						new Task().id(2L).text("t2").status(new CodeBookItem().code("COMPLETED"))
				).collect(Collectors.toList()));

		mockMvc.perform(get(String.format("/partners/%s/tasks", partnerId)))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].text", is("t1")))
				.andExpect(jsonPath("$[0].status.code", is("ACTIVE")))
				.andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].text", is("t2")))
				.andExpect(jsonPath("$[1].status.code", is("COMPLETED")));

		verify(taskFacade).getTasks(partnerId);
	}
}
