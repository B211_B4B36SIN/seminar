package cz.cvut.fel.demoapp.be.repository;

import cz.cvut.fel.demoapp.be.DemoappApplication;
import cz.cvut.fel.demoapp.be.model.CbStatus;
import cz.cvut.fel.demoapp.be.model.Task;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class TaskRepositoryTest {

	public static final String PARTNER_123456789 = "123456789";
	public static final String PARTNER_111111111 = "111111111";
	public static final String PARTNER_NOT_FOUND_ID = "777";

	@Autowired
	private TaskRepository repository;

	private Task task1;

	@BeforeEach
	public void setUp() {
		this.task1 = save(PARTNER_123456789, CbStatus.COMPLETED, "Task 1");
		save(PARTNER_111111111, CbStatus.ACTIVE, "Task 2");
		save(PARTNER_123456789, CbStatus.ACTIVE, "Task 3");
	}

	@Test
	public void testFindByIdAndPartnerId() {
		Task task = repository.findByIdAndPartnerId(task1.getId(), "123456789");
		validate(task, PARTNER_123456789, CbStatus.COMPLETED, "Task 1");
	}

	@Test
	public void testFindByPartnerIdOrderByCreatedDate() {
		List<Task> tasks = repository.findByPartnerIdOrderByCreatedDate("123456789").collect(Collectors.toList());
		assertSame(2, tasks.size());
		validate(tasks.get(0), PARTNER_123456789, CbStatus.COMPLETED, "Task 1");
		validate(tasks.get(1), PARTNER_123456789, CbStatus.ACTIVE, "Task 3");
	}

	@Test
	public void testDeleteByPartnerIdAndStatus() {
		repository.deleteByPartnerIdAndStatus(PARTNER_123456789, CbStatus.ACTIVE);
		List<Task> tasks = repository.findByPartnerIdOrderByCreatedDate(PARTNER_123456789).collect(Collectors.toList());
		assertSame(1, tasks.size());
		validate(tasks.get(0), PARTNER_123456789, CbStatus.COMPLETED, "Task 1");
	}

	@Test
	public void testExistsByPartnerId() {
		assertTrue(repository.existsByPartnerId(PARTNER_123456789));
		assertFalse(repository.existsByPartnerId(PARTNER_NOT_FOUND_ID));
	}

	@Test
	public void testExistsByPartnerIdAndId() {
		assertTrue(repository.existsByPartnerIdAndId(PARTNER_123456789, task1.getId()));
		assertFalse(repository.existsByPartnerIdAndId(PARTNER_NOT_FOUND_ID, task1.getId()));
	}

	private void validate(Task task, String partnerId, CbStatus status, String text) {
		assertSame(partnerId, task.getPartnerId());
		assertSame(status, task.getStatus());
		assertSame(text, task.getText());
	}

	private Task save(String partnerId, CbStatus status, String text) {
		Task task = new Task();
		task.setPartnerId(partnerId);
		task.setStatus(status);
		task.setText(text);
		return repository.save(task);
	}
}
