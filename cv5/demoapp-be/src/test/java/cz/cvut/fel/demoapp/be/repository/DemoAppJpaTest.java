package cz.cvut.fel.demoapp.be.repository;


import cz.cvut.fel.demoapp.be.model.DemoAppDomainJpaPackage;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@DataJpaTest
@EntityScan(basePackageClasses = DemoAppDomainJpaPackage.class)
@EnableJpaRepositories(basePackageClasses = DemoAppBusinessRepositoryPackage.class)
@ContextConfiguration(classes = RepositoryConfigurationTest.class)
public @interface DemoAppJpaTest {
}
