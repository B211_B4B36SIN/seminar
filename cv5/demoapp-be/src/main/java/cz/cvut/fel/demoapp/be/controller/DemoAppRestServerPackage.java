package cz.cvut.fel.demoapp.be.controller;

/**
 * The sole purpose of this class is to represent its package in order to allow for a type-safe component scanning.
 */
public class DemoAppRestServerPackage {

	private DemoAppRestServerPackage() {
		// Nothing to do here.
	}
}
