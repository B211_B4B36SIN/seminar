package cz.cvut.fel.demoapp.be.repository;

/**
 * The sole purpose of this class is to represent its package in order to allow for a type-safe component scanning.
 */
public class DemoAppBusinessRepositoryPackage {

    private DemoAppBusinessRepositoryPackage() {
        // Nothing to do here.
    }
}
