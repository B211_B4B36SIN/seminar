package cz.cvut.fel.demoapp.be.controller.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeartbeatController {

    @GetMapping("/echo")
    public String test() {
        return "It works!";
    }
}
