package cz.cvut.fel.demoapp.be.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class TaskUpdateStatus {

    @NotNull
    @Valid
    private CodeBookItemIdentifier status;

}
