package cz.cvut.fel.demoapp.be.util;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static Date toDate(String date, String format) {
        if (date == null) {
            return null;
        }
        return Date.valueOf(LocalDate.parse(date, DateTimeFormatter.ofPattern(format)));
    }

    public static Date toDate(LocalDate localDate) {
        if (localDate == null) {
            return null;
        }
        return Date.valueOf(localDate);
    }

    public static String format(Date date, String format) {
        if (date == null) {
            return null;
        }
        return date.toLocalDate().format(DateTimeFormatter.ofPattern(format));
    }

    public static String format(Timestamp timestamp, String format) {
        if (timestamp == null) {
            return null;
        }
        return timestamp.toLocalDateTime().format(DateTimeFormatter.ofPattern(format));
    }
}
