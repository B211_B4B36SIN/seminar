package cz.cvut.fel.demoapp.be.config;

import cz.cvut.fel.demoapp.be.controller.ControllerPackage;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${swagger.pathMapping:#{null}}")
    private String pathMapping;

    private ApiInfo DEFAULT_API_INFO = new ApiInfo(
            "demoapp", null, "1.0",
            null, null,
            null, null, new ArrayList<>());

    @Bean
    public Docket api() {
       return new Docket(DocumentationType.SWAGGER_2)
                .pathMapping(pathMapping)
                .apiInfo(DEFAULT_API_INFO)
                .select()
                .apis(RequestHandlerSelectors.basePackage(ControllerPackage.class.getPackage().getName()))
                .paths(PathSelectors.any())
                .build();
    }
}
