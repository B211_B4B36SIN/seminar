package cz.cvut.fel.demoapp.be.controller.config;


import cz.cvut.fel.demoapp.be.exception.FieldInvalidException;
import cz.cvut.fel.demoapp.be.exception.FieldMissingException;
import cz.cvut.fel.demoapp.be.exception.FieldTooLongException;
import cz.cvut.fel.demoapp.be.exception.NotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.OffsetDateTime;
import java.util.UUID;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	public static final String FIELD_MISSING = "FIELD_MISSING";
	public static final String FIELD_INVALID = "FIELD_INVALID";
	public static final String FIELD_TOO_LONG = "FIELD_TOO_LONG";

	@ExceptionHandler(value = {NotFoundException.class})
	protected ResponseEntity<Object> handleNotFoundException(NotFoundException ex) {
		Error error = errorResponse();
		//error.setCode(ex.getErrorCode());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
	}

	@ExceptionHandler(value = {FieldMissingException.class})
	protected ResponseEntity<Object> handleFieldMissingException(FieldMissingException ex) {
		return badRequest(FIELD_MISSING);
	}

	@ExceptionHandler(value = {FieldInvalidException.class})
	protected ResponseEntity<Object> handleFieldInvalidException(FieldInvalidException ex) {
		return badRequest(FIELD_INVALID);
	}

	@ExceptionHandler(value = {FieldTooLongException.class})
	protected ResponseEntity<Object> handleFieldTooLongException(FieldTooLongException ex) {
		return badRequest(FIELD_TOO_LONG);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
																  HttpHeaders headers,
																  HttpStatus status,
																  WebRequest request) {
		Error err = errorResponse();
		return badRequest(err);
	}

	private ResponseEntity<Object> badRequest(Error error) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
	}

	private ResponseEntity<Object> badRequest(String code) {
		Error error = errorResponse();
		//error.setCode(code);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
	}

	private Error errorResponse() {
		Error error = new Error();
		//error.setTime(OffsetDateTime.now());
		//error.setUuid(getUuid());
		return error;
	}

	private String getUuid() {
		return UUID.randomUUID().toString();
	}
}
