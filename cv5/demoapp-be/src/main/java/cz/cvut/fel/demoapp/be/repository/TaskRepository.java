package cz.cvut.fel.demoapp.be.repository;

import cz.cvut.fel.demoapp.be.model.CbStatus;
import cz.cvut.fel.demoapp.be.model.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.stream.Stream;

public interface TaskRepository extends CrudRepository<Task, Long> {

    Stream<Task> findByPartnerIdOrderByCreatedDate(String partnerId);

    void deleteByPartnerIdAndStatus(String partnerId, CbStatus status);

    Task findByIdAndPartnerId(long id, String partnerId);

    boolean existsByPartnerId(String partnerId);

    boolean existsByPartnerIdAndId(String partnerId, long id);
}
