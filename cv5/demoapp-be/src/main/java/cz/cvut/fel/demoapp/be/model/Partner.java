package cz.cvut.fel.demoapp.be.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "PARTNER")
public class Partner extends AbstractEntity {

    @Column(name = "FIRSTNAME")
    private String firstName;
    @Column(name = "LASTNAME")
    private String lastName;
    @Column(name = "DATEOFBIRTH")
    private String dateOfBirth;
    @Column(name = "PERSONNUMBER")
    private Integer personNumber;

}
