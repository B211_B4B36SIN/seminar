drop sequence DEMOAPP_SEQUENCE;
drop table TASK;

create sequence DEMOAPP_SEQUENCE start with 1 increment by 1;

create table TASK (
    ID                   number(10) unique not null,
    CREATED_DATE         timestamp,
    LAST_MODIFIED_DATE   timestamp,
    VERSION              number(10),
    PARTNER_ID           varchar(50 char),
    STATUS_CODE          varchar(50 char),
    TEXT                 varchar(2000 char)
);

insert into TASK (ID, CREATED_DATE, LAST_MODIFIED_DATE, VERSION, PARTNER_ID, STATUS_CODE, TEXT)
values (DEMOAPP_SEQUENCE.nextval, sysdate, sysdate, 0,
        '111', 'ACTIVE', 'Identify resources to be monitored');

insert into TASK (ID, CREATED_DATE, LAST_MODIFIED_DATE, VERSION, PARTNER_ID, STATUS_CODE, TEXT)
values (DEMOAPP_SEQUENCE.nextval, sysdate, sysdate, 0,
        '111', 'ACTIVE', 'Define users and workflow');

insert into TASK (ID, CREATED_DATE, LAST_MODIFIED_DATE, VERSION, PARTNER_ID, STATUS_CODE, TEXT)
values (DEMOAPP_SEQUENCE.nextval, sysdate, sysdate, 0,
        '111', 'ACTIVE', 'Identify event sources by resource type');

insert into TASK (ID, CREATED_DATE, LAST_MODIFIED_DATE, VERSION, PARTNER_ID, STATUS_CODE, TEXT)
values (DEMOAPP_SEQUENCE.nextval, sysdate, sysdate, 0,
        '222', 'COMPLETED', 'Define the relationship between resources and business systems');